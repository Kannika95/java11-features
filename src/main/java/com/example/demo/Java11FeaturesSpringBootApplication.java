package com.example.demo;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.WebSocket;
import java.net.http.WebSocket.Builder;
import java.net.http.WebSocket.Listener;
import java.nio.ByteBuffer;
import java.nio.file.Files;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.util.Arrays;
//import java.util.List;
//import java.util.stream.Collectors;
//
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collection;
//import java.util.List;
//import java.util.concurrent.ConcurrentLinkedQueue;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;




public class Java11FeaturesSpringBootApplication {
	public static void main(String[] args) throws IOException, InterruptedException {

		httpGet();
		httpPost();
		//		httpPut();
		//		httpDelete();

		//				System.out.println("This code is executed without explicit compilation!");
		//						String sample="  ";
		//				
		//				String stripString="    STRIP   ";
		//				
		//						System.out.println("Repeat string n number of times: " + sample.repeat(5));
		//						Map<String,String> map=new HashMap<>();
		//						map.put(null, null);
		//						map.put(null, null);
		//				System.out.println(map);
		//				System.out.println(sample.isBlank());
		//		
		//				System.out.println("Leading and trailing whitespaces removed: "+stripString.strip());
		//		
		//				System.out.println("Removes the leading whitespaces: "+stripString.stripLeading());//  "STRIP "
		//		
		//				System.out.println("Removes the trailing  whitespaces: "+stripString.stripTrailing()); // " STRIP"
		//		
		//						String string = "STG\rin\rChennai";
		//				      string.lines().forEach(a->System.out.println(a));

		//				Path createdFile = Files.createTempFile("text", ".txt");
		//				Path path = Files.writeString(createdFile, "The readString() and writeString() are static methods");
		//				System.out.println(path);
		//				String s = Files.readString(path);
		//				System.out.println(s); 

		//						 Cal cal = (var a,  var b)-> a+b;		
		//						 int result = cal.sum(10, 20);
		//						 System.out.println(result);

		//				 List<String> languageList = Arrays.asList("Java", "HTML");
		//			      String tutorials = languageList.stream()
		//			         .map((@NonNull var tutorial) -> tutorial.toUpperCase())
		//			         .collect(Collectors.joining(", "));
		//			      System.out.println(tutorials);

		//				List<Integer> list = new ArrayList<>();
		//				list.add(5);
		//				list.add(10);
		//				list.add(15);
		//				Integer[] intArr = list.toArray(Integer[]::new);
		//				System.out.print("Converting List to Array: "+Arrays.toString(intArr));
		//
		//				Collection<Integer>collection= new ConcurrentLinkedQueue<>();  
		//				for (int i=1;i<=10;i++) {  
		//					collection.add(i);  
		//				}  
		//				Integer[] integerArray =  collection.toArray(Integer[]::new);  
		//				System.out.print("\nConverting List to Array: "+Arrays.toString(integerArray));
		//		
		//		List<String> namesList = Arrays.asList("Java", "Angular");
		//		String[] names = namesList.toArray(String[]::new);
		//		System.out.println("\nLength of the string array is: "+names.length);

	}

	private static void httpDelete() {
		HttpClient httpClient = HttpClient.newBuilder()
				.version(HttpClient.Version.HTTP_1_1)
				.connectTimeout(Duration.ofSeconds(10))
				.build(); 
		HttpRequest httpRequest = HttpRequest
				.newBuilder(URI.create("http://localhost:8080/api/delete/8"))
				.header("Content-Type", "application/json")
				.DELETE()
				.build();
		try {
			HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
			System.out.println("The status of DELETE request: " + response.statusCode());
			System.out.println("The updated data response body : " + response.body());
			System.out.println("Version: " + response.version());
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

	}

	private static void httpPut() {
		HttpClient httpClient = HttpClient.newBuilder()
				.version(HttpClient.Version.HTTP_1_1)
				.connectTimeout(Duration.ofSeconds(10))
				.build(); 
		HttpRequest httpRequest = HttpRequest
				.newBuilder(URI.create("http://localhost:8080/api/update/8"))
				.header("Content-Type", "application/json")
				.PUT(HttpRequest.BodyPublishers
						.ofString("{\n"
								+ "    \"userName\": \"Kannika\",\n"
								+ "    \"password\": \"1234\",\n"
								+ "    \"address\": \"Mannai\",\n"
								+ "    \"contact\": 12345678,\n"
								+ "    \"email\": \"kann@chennai.com\"\n"
								+ "}"))
				.build();
		try {
			HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
			System.out.println("The status of PUT request: " + response.statusCode());
			System.out.println("The updated data response body : " + response.body());
			System.out.println("Version: " + response.version());
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

	}

	private static void httpPost() {
		HttpClient httpClient = HttpClient.newBuilder()
				.version(HttpClient.Version.HTTP_1_1)
				.connectTimeout(Duration.ofSeconds(10))
				.build(); 
		HttpRequest httpRequest = HttpRequest
				.newBuilder(URI.create("http://localhost:8080/api/save"))
				.header("Content-Type", "application/json")
				.POST(HttpRequest.BodyPublishers
						.ofString("{\n"
								+ "    \"userName\": \"Ramesh\",\n"
								+ "    \"password\": \"1234\",\n"
								+ "    \"address\": \"chennai\",\n"
								+ "    \"contact\": 12345678,\n"
								+ "    \"email\": \"ram@chennai.com\"\n"
								+ "}"))
				.build();
		try {
			HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
			System.out.println("The status of POST request: " + response.statusCode());
			System.out.println("The saved data response body : " + response.body());
			System.out.println("Version: " + response.version());
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void httpGet() {
		HttpClient httpClient = HttpClient.newBuilder()
				.version(HttpClient.Version.HTTP_2)
				.connectTimeout(Duration.ofSeconds(10))
				.build();

		try {
			System.exit(0);
		}catch(Exception exception) {

		}finally {
			System.out.println("finally execute");
		}
		//		try {
		//			HttpRequest request = HttpRequest.newBuilder()
		//					.GET()
		//					.uri(URI.create("http://localhost:8080/api/getAll"))
		//					.build();                              
		//			HttpResponse<String> response = httpClient.send(request,
		//					HttpResponse.BodyHandlers.ofString()); 
		//
		//			System.out.println("Status code of GET request: " + response.statusCode());                            
		//			System.out.println("Headers: " + response.headers().allValues("content-type"));
		//			System.out.println("Getting all data: " + response.body());
		//		} catch (IOException | InterruptedException e) {
		//			e.printStackTrace();
		//		}
	}  

}
