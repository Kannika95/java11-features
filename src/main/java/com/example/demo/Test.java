package com.example.demo;

import java.util.Arrays;
import java.util.stream.Collectors;


import com.example.demo.Test.Nest2.Nested1ClassA;
import com.example.demo.Test.Nest2.Nested1ClassB;
public interface Test {
	private static void getNestHost() {
//        System.out.println("Nest Host:"+Nested1ClassB.class.getNestHost().getSimpleName());
		
//        Class<?>[] nestMembers = Nested1ClassB.class.getNestMembers();
//        System.out.println("Nest Members:\n" +
//                Arrays.stream(nestMembers).map(Class::getSimpleName)
//                        .collect(Collectors.joining("\n")));
//        
        System.out.println("Nest Mate:\n" +Nested1ClassA.class.isNestmateOf(NestCheck.class));
    }

    public static void main(String[] args) {
        getNestHost();
    }
    public class Nest1 {
    }
    public class Nest2 {
        public interface Nested1ClassA {
        	default String get() {
        		return "Hello";
        	}
        }
        public class Nested1ClassB {
        }
    }
}